import { LitElement, html } from "lit-element";

class FichaPersona extends LitElement {
  static get properties() {
    return {
      name: { type: String },
      yearsInCompany: { type: Number },
      personinfo: { type: String }
    }
  }
  constructor() {
    super();

    this.name = "prueba nombre";
    this.yearsInCompany = 12;
    this.updatePersonInfo();
  }
  updated(changedProperties) {
    console.log("updated");

    changedProperties.forEach((oldValue, propName) => {
      console.log("Propiedad " + propName + "cambia valor, anterior era " + oldValue);
    });

    if (changedProperties.has("name")) {
      console.log("propiedad name cambia valor antes era "
        + changedProperties.get("name") + "nuevo es " + this.name);
    }
    if (changedProperties.has("yearsInCompany")) {
      console.log("propiedad yearsincompany cambia valor antes era "
        + changedProperties.get("yearsInCompany") + "nuevo es " + this.yearsInCompany);
      this.updatePersonInfo();
    }
  }
  render() {
    return html`
      <div>
        <label>Nombre Completo</label>
        <input type = "text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
        <br />
        <label>Años en la empresa</label>
        <input type = "text" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
        <br />
        <input type = "text" value ="${this.personinfo}" disabled></input>
        <br />
        <button>Enviar</button>
      </div >
      `;
  }
  updateName(e) {
    console.log("updateName");
    this.name = e.target.value;
  }
  updateYearsInCompany(e) {
    console.log("updateYearsInCompany");
    this.yearsInCompany = e.target.value;

  }
  updatePersonInfo() {
    console.log("update person info")
    if (this.yearsInCompany >= 7) {
      this.personinfo = "lead";
    } else if (this.yearsInCompany >= 5) {
      this.personinfo = "Senior";
    } else if (this.yearsInCompany >= 3) {
      this.personinfo = "team";
    } else {
      this.personinfo = "junior";
    }
  }
}

customElements.define('ficha-persona', FichaPersona);
